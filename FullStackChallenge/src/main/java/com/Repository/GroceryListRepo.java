package com.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Model.GroceryList;

@Repository
public interface GroceryListRepo extends JpaRepository<GroceryList,Long>{

}
