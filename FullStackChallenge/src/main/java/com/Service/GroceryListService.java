package com.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Model.GroceryItem;
import com.Model.GroceryList;
import com.Repository.GroceryItemRepo;
import com.Repository.GroceryListRepo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryListService {
	private GroceryListRepo listRepo;
	private GroceryItemRepo itemRepo;
	

	public void createGroceryList(String name) {
		GroceryList list=new GroceryList(name);
		listRepo.save(list);
	}

	public void addItemToGroceryList(long listId, long itemId) {
		GroceryList listToAdd=listRepo.getOne(listId);
		GroceryItem itemToAdd=itemRepo.getOne(itemId);
		listToAdd.addItemToList(itemToAdd);
		listRepo.save(listToAdd);
	}

	public List<GroceryList> getAllGroceryLists(){
		return listRepo.findAll();
	}

	public void removeItemFromGroceryList(long listId, long itemId) {
		GroceryList listToRemove=listRepo.getOne(listId);
		GroceryItem itemToRemove=itemRepo.getOne(itemId);
		listToRemove.removeItemFromList(itemToRemove);
		listRepo.save(listToRemove);
		
		
	}

	public void removeGroceryList(long listId) {
		listRepo.deleteById(listId);
	}

}
