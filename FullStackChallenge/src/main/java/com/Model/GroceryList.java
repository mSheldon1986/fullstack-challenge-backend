package com.Model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroceryList {
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column
	private String name;
	
	@OneToMany
	private List<GroceryItem> list;
	
	public GroceryList(String name) {
		this.name=name;
	}

	public void addItemToList(GroceryItem toAdd) {
		list.add(toAdd);
	}
	public void removeItemFromList(GroceryItem toRemove) {
		list.remove(toRemove);
	}
}
