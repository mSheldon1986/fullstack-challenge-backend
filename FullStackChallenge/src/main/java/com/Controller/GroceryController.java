package com.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Model.GroceryList;
import com.Service.GroceryListService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value="grocery-lists")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryController {
	private GroceryListService serv;
	
	@GetMapping
	public ResponseEntity<List<GroceryList>> getAllLists(){
		return new ResponseEntity<List<GroceryList>>(serv.getAllGroceryLists(),HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<String> createNewList(@RequestBody Map<String, String> map){
		if(map.get("name")==null) {
			map.put("name", "Default");
		}
		serv.createGroceryList(map.get("name"));
		return new ResponseEntity<>("New GroceryList Created",HttpStatus.CREATED);
	}
	@PostMapping("/items")
	public ResponseEntity<String> addItemToList(@RequestBody Map<String, String> map){
		long listId=Long.parseLong(map.get("listId"));
		long itemId=Long.parseLong(map.get("itemId"));
		serv.addItemToGroceryList(listId, itemId);
		return new ResponseEntity<>("Added Item with Id: "+itemId+" to List of Id: "+listId,	HttpStatus.CREATED);
	}
	@DeleteMapping("/items/{itemid}")
	public ResponseEntity<String> removeItemFromList(@PathVariable("itemid") Long itemId, @RequestBody Map<String, String> map){
		System.out.println(map);
		System.out.println(itemId);
		long listId=Long.parseLong(map.get("listId"));
		serv.removeItemFromGroceryList(listId, itemId);
		return new ResponseEntity<>("Item with Id: "+itemId+" deleted from list with Id: "+listId,HttpStatus.ACCEPTED);
	}
	@DeleteMapping
	public ResponseEntity<String> removeList(@RequestBody Map<String, String> map){
		long listId=Long.parseLong(map.get("listId"));
		serv.removeGroceryList(listId);
		return new ResponseEntity<>("List with Id: "+listId+" deleted.",HttpStatus.ACCEPTED);
	}

}
